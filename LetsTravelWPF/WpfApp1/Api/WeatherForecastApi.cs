﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RestSharp;

namespace WpfApp1.Api
{
    class WeatherForecastApi
    {
        public async Task<Model.OpenWeatherModels.OpenForecast.Example> getWeather(string nameCity)
        {
            try
            {
                var client = new RestClient("https://localhost:44344/");
                var request = new RestRequest("api/Weather", Method.GET);
                request.AddParameter("city",nameCity);
                request.AddHeader("Authorization", "Bearer " + GlobalVar.StaticVar.AccessToken);

                var response = await client.ExecuteGetTaskAsync<Model.OpenWeatherModels.OpenForecast.Example>(request);
                //var da =SimpleJson. response.Content;
                var model = SimpleJson.DeserializeObject<Model.OpenWeatherModels.OpenForecast.Example> (response.Content);
               // MessageBox.Show(response.Content+response.StatusCode);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return model;
                }
            }
            catch (Exception e) { }
            return null;
        }
    }
}
