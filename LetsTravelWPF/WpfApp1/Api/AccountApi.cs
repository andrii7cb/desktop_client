﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Windows;
using RestSharp;
using System.Net;

[assembly: InternalsVisibleTo("LetsTravelWPFTests")]

namespace WpfApp1.Api
{
    public class AccountApi
    {
        async public Task<string> Login(string username, string password)
        {
            try
            {

                var client = new RestClient("https://localhost:44344");
                var request = new RestRequest("/Token", Method.POST);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("grant_type", "password");
                request.AddParameter("username", username);
                request.AddParameter("password", password);

                var response = await client.ExecuteTaskAsync(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var ar = response.Content.Split('"');
                    GlobalVar.StaticVar.AccessToken = ar[3];
                    MessageBox.Show(GlobalVar.StaticVar.AccessToken);
                }
                return response.StatusCode.ToString();

            }
            catch (Exception e)
            {
                MessageBox.Show("Error" + e.Message);
            }
            return "no connect to server";
        }

        async public Task<string> RecoverPassword(string password)
        {
            try
            {

                var client = new RestClient("https://localhost:44344");
                var request = new RestRequest("/api/Account/ForgotPassword", Method.POST);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("grant_type", "password");
                request.AddParameter("Password", password);

                var response = await client.ExecuteTaskAsync(request);
                // if (response.StatusCode == HttpStatusCode.OK) { }
                MessageBox.Show(response.Content + response.Cookies + response.Headers + response.StatusCode);

                return response.StatusCode.ToString();

            }
            catch (Exception e)
            {
                MessageBox.Show("Error" + e.Message);
            }
            return "no connect to server";
        }

        async public Task<string> Register(string username, string password, string confirmPassword)
        {
            try
            {

                var client = new RestClient("https://localhost:44344");
                var request = new RestRequest("/api/Account/Register", Method.POST);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("grant_type", "password");
                request.AddParameter("Email", username);
                request.AddParameter("Password", password);
                request.AddParameter("ConfirmPassword", confirmPassword);

                var response = await client.ExecuteTaskAsync(request);
                // if (response.StatusCode == HttpStatusCode.OK) { }
                MessageBox.Show(response.Content + response.Cookies + response.Headers + response.StatusCode);

                return response.StatusCode.ToString();

            }
            catch (Exception e)
            {
                MessageBox.Show("Error" + e.Message);
            }
            return "no connect to server";
        }


    }
}
