﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for WheatherWindow.xaml
    /// </summary>
    public partial class WheatherWindow : Window
    {
        public WheatherWindow()
        {
            InitializeComponent();
           // wheatherBrouser.Navigate("C:\\Users\\adminn.admin\\Source\\Repos\\EleksHome7\\LetsTravelWPF\\WpfApp1\\WheatherForecast.html");
            //wheatherBrouser.ObjectForScripting = true;
         
        //    wheatherBrouser.BeginInit();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            Api.WeatherForecastApi weather = new Api.WeatherForecastApi();
        var model = await    weather.getWeather(cityWeather.Text);
            richTextWeather.AppendText(model.message+model.ToString());
            var arrList = model.list;
            foreach (Model.OpenWeatherModels.OpenForecast.List l in arrList) {
                richTextWeather.AppendText(l.humidity+"___"+l.clouds + "___" +l.deg + "___" +l.dt + "___" + l.pressure + "___" +l.rain+"___" +l.speed + "___" + "___" +l.temp.max + "___" +l.temp.night);
            }
        }
    }
}
