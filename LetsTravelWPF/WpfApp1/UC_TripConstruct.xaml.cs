﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for UC_TripConstruct.xaml
    /// </summary>
    public partial class UC_TripConstruct : UserControl
    {
        public UC_TripConstruct()
        {
            InitializeComponent();
        }

        private void DescriptionLink_Click(object sender, RoutedEventArgs e)
        {
            PlaceDescription placeDescr = new PlaceDescription();
            placeDescr.ShowDialog();
        }
    }
}
