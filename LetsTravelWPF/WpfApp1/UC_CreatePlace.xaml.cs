﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for UC_CreatePlace.xaml
    /// </summary>
    public partial class UC_CreatePlace : UserControl
    {
        Api.GoogleApi googleApi = new Api.GoogleApi();
      
          public UC_CreatePlace()
        {
            InitializeComponent();
        }
        private void BackToTripButton_Click(object sender, RoutedEventArgs e)
        {
            //MainWindow.contentControl.Content = new UC_CreateTrip();
            this.Content = new UC_CreateTrip();
        }
        private async void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            Model.PlaceApi placeApi = new Model.PlaceApi();
            //placeApi.CityId =  textBoxCityName.Text;
            placeApi.Name = textBoxPlaceName.Text;
            //placeApi. = textBoxPlaceAddress.Text;
            placeApi.WorkingHours = "10.00";//textBoxWorkingHours.Text;
            placeApi.DurationInSeconds = 200; //Double.Parse(textBoxDuration.Text);
            placeApi.Latitude = Double.Parse( textBoxLatitude.Text);
            placeApi.Longitude = Double.Parse( textBoxLongitude.Text);
            placeApi.Description = textBoxDescription.Text;
          var answer = await GlobalVar.StaticVar.UniversalApi.SaveAsync<Model.PlaceApi>(placeApi);
            //+send foto!!!!!!!!!!!!!!
        }


        private void PhotoBrowseButton_Click(object sender, RoutedEventArgs e)
        {
            //MainWindow.contentControl.Content = new UC_CreateTrip();
            //this.Content = new UC_CreateTrip();
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.ShowDialog();

            if (fileDialog.CheckFileExists && fileDialog.CheckPathExists)
                textBoxPlacePhoto.Text = fileDialog.FileName;
        }

        private async void textBoxPlaceName_ManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            Api.GoogleApi googleApi = new Api.GoogleApi();
            MessageBox.Show("start req");
            string[] arr = await googleApi.ReadAll(ComboBoxPlaceName.Text);
            for (int i=0;i<arr.Length; i++ ) { 
            ComboBoxPlaceName.Items.Add(arr[i]);//
        }
        }
        private async void ComboBoxPlaceName_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
               
                string[] arr = await googleApi.ReadAll(ComboBoxPlaceName.Text);
                for (int i = 0; i < arr.Length; i++)
                {
                    ComboBoxPlaceName.Items.Add(arr[i]);
                }
                ComboBoxPlaceName.IsDropDownOpen = true;


            }
        }

        private void ComboBoxPlaceName_Selected(object sender, RoutedEventArgs e)
        {
           // var a = (ComboBox)sender;
           //var t = a.SelectedIndex;
           // MessageBox.Show("____"+t);

        }
    }
}
