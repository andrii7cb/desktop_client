﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for UC_StartView.xaml
    /// </summary>
    public partial class UC_StartView : UserControl
    {
        public UC_StartView()
        {
            InitializeComponent();
        }
        private void BuildTripButton_Click(object sender, RoutedEventArgs e)
        {
           this.Content = new UC_CreateTrip();
        }
        private void ChooseTripButton_Click(object sender, RoutedEventArgs e)
        {
            this.Content = new UC_ChooseTrip();
        }
    }
}
