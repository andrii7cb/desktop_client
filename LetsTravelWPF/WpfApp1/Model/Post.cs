﻿using System;

namespace WpfApp1.Model
{
    class Post
    {
        public int Id { get; set; }
        public string UserLogin { get; set; }
        public string UserPhoto { get; set; }
        public string PostMessage { get; set; }
        public string AddedPhoto { get; set; }
        public DateTime DateTime { get; set; }
        public int Rating { get; set; }
        public int TripId { get; set; }
        public virtual Trip Trip { get; set; }
    }
}
