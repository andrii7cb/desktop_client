﻿using System.Runtime.Serialization;

namespace WpfApp1.Model
{[DataContract]
    public class Country
    {
        
        [IgnoreDataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }
        //public virtual List<State> StateList { get; set; }
        public Country() { }
    }
}
