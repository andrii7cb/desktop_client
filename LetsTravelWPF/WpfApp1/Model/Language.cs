﻿using System.Collections.Generic;

namespace WpfApp1.Model
{
    class Language
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<User> UserList { get; set; }
    }
}
