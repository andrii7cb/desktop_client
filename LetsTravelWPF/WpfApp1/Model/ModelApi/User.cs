﻿using System;
using System.Collections.Generic;

namespace WpfApp1.Model
{
    class UserApis
    {

//        [Required(ErrorMessage = "First Name is required")]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
  //      [Required(ErrorMessage = "Last Name is required")]
        public string LastName { get; set; }
   //     [Required(ErrorMessage = "Login is required")]
        public string Login { get; set; }
     //   [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
        public string Photo { get; set; }
        public string PreferableThings { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime BirthDate { get; set; }
        public bool IsMan { get; set; }
      //  [Required(ErrorMessage = "Phone Number is required")]
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
      // [Required(ErrorMessage = "Email is required")]
       //[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email is not valid.")]
        public string Email { get; set; }
        public int Rating { get; set; }
        public virtual Address Address { get; set; }
      
        

    }
}
