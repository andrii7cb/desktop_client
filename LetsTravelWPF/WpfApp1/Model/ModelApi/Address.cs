﻿namespace WpfApp1.Model.ModelApi
{
    class AddressApi
    {
        
        public int UserId { get; set; }
       // [Required(ErrorMessage = "Country is required")]
        public string Country { get; set; }
       // [Required(ErrorMessage = "State is required")]
        public string State { get; set; }
       // [Required(ErrorMessage = "City is required")]
        public string City { get; set; }
       // [Required(ErrorMessage = "Street is required")]
        public string Street { get; set; }
       // [Required(ErrorMessage = "Building Number is required")]
        public string BuildingNumber { get; set; }
       // [Required(ErrorMessage = "Postal/zip code is required")]
        public string PostalZipCode { get; set; }
        public virtual User User { get; set; }
    }
}
